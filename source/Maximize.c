// Preamble {{{
// =============================================================================
//        File: Maximize.c
// -----------------------------------------------------------------------------
//     Version: 0.0.0
// -----------------------------------------------------------------------------
//     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
// -----------------------------------------------------------------------------
// Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
// -----------------------------------------------------------------------------
//  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
// -----------------------------------------------------------------------------
//     License: This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be useful,
//              but WITHOUT ANY WARRANTY; without even the implied warranty of
//              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//              General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program. If not, see
//              <http://www.gnu.org/licenses/>.
// =============================================================================
// }}} Preamble

// Preprocessor {{{
// =============================================================================
// Includes {{{
// =============================================================================
#include "GetMainWindow"

#include <assert.h>
#include <stdbool.h>
// =============================================================================
// }}} Includes

// Definitions {{{
// =============================================================================
#define DEMANGLED         Vim_MWM_DEMANGLED
#define PUBLIC            Vim_MWM_PUBLIC
#define PRIVATE           Vim_MWM_PRIVATE
#define UNUSED            Vim_MWM_UNUSED
#define E                 Vim_MWM_ERROR
#define ERROR_DECLARATION Vim_MWM_ERROR_DECLARATION
#define TRACE             Vim_MWM_TRACE
#define TRACE_LENGTH      Vim_MWM_TRACE_LENGTH
#define TRACE_DECLARATION Vim_MWM_TRACE_DECLARATION

#define GetMainWindow Vim_MWM_GetMainWindow

#define Error_NONE      Vim_MWM_Error_NONE
#define Error_CREATE    Vim_MWM_Error_CREATE
#define Error_THROW     Vim_MWM_Error_THROW
#define Error_PROPAGATE Vim_MWM_Error_PROPAGATE

#define Error_ToTrace Vim_MWM_Error_ToTrace

#define Error_Code_WINDOW Vim_MWM_Error_Code_WINDOW
// =============================================================================
// }}} Definitions
// =============================================================================
// }}} Preprocessor

// Typedefs {{{
// =============================================================================
typedef Vim_MWM_Error Error;
// =============================================================================
// }}} Typedefs

// Functions {{{
// =============================================================================
// Declarations {{{
// =============================================================================
// External {{{
// =============================================================================
DEMANGLED
char const*
__cdecl
PUBLIC
Vim_MWM_Maximize(long argument);

DEMANGLED
char const*
__cdecl
PUBLIC
Vim_MWM_SetMaximized(long maximized);

DEMANGLED
char const*
__cdecl
PUBLIC
Vim_MWM_IsMaximized(long argument);

DEMANGLED
char const*
__cdecl
PUBLIC
Vim_MWM_ToggleMaximized(long argument);
// =============================================================================
// }}} External

// Internal {{{
// =============================================================================
static
Error const*
PRIVATE
SetMaximized(HWND window, bool maximized);

static
Error const*
PRIVATE
IsMaximized(HWND window, bool* maximized);

static
Error const*
PRIVATE
ToggleMaximized(HWND window);
// =============================================================================
// }}} Internal
// =============================================================================
// }}} Declarations

// Definitions {{{
// =============================================================================
// External {{{
// =============================================================================
char const*
Vim_MWM_Maximize(long argument) {
  UNUSED(argument);

  return Vim_MWM_SetMaximized(1);
}

char const*
Vim_MWM_SetMaximized(long maximized) {
  ERROR_DECLARATION;
  TRACE_DECLARATION;

  HWND main_window;

  if ((E = GetMainWindow(&main_window))) {
    Error_ToTrace(&Error_CREATE(E->code, E->message, E), TRACE, TRACE_LENGTH);

    return TRACE;
  }

  if ((E = SetMaximized(main_window, maximized))) {
    Error_ToTrace(&Error_CREATE(E->code,
                                "Cannot set maximized for main window.",
                                E),
                  TRACE,
                  TRACE_LENGTH);

    return TRACE;
  }

  return NULL;
}

char const*
Vim_MWM_IsMaximized(long argument) {
  UNUSED(argument);

  ERROR_DECLARATION;
  TRACE_DECLARATION;

  HWND main_window;

  if ((E = GetMainWindow(&main_window))) {
    Error_ToTrace(&Error_CREATE(E->code, E->message, E), TRACE, TRACE_LENGTH);

    return TRACE;
  }

  bool maximized;

  if ((E = IsMaximized(main_window, &maximized))) {
    Error_ToTrace(
      &Error_CREATE(E->code,
                    "Cannot determine whether main window is maximized.",
                    E),
      TRACE,
      TRACE_LENGTH);

    return TRACE;
  }

  if (maximized) {
    return Vim_MWM_TRUE;
  }

  return Vim_MWM_FALSE;
}

char const*
Vim_MWM_ToggleMaximized(long argument) {
  UNUSED(argument);

  ERROR_DECLARATION;
  TRACE_DECLARATION;

  HWND main_window;

  if ((E = GetMainWindow(&main_window))) {
    Error_ToTrace(&Error_CREATE(E->code, E->message, E), TRACE, TRACE_LENGTH);

    return TRACE;
  }

  if ((E = ToggleMaximized(main_window))) {
    Error_ToTrace(&Error_CREATE(E->code,
                                "Cannot toggle maximized for main window.",
                                E),
                  TRACE,
                  TRACE_LENGTH);

    return TRACE;
  }

  return NULL;
}
// =============================================================================
// }}} External

// Internal {{{
// =============================================================================
Error const*
SetMaximized(HWND window, bool maximized) {
  assert(window != 0);

  ERROR_DECLARATION;

  bool already_maximized;

  if ((E = IsMaximized(window, &already_maximized))) {
    Error_PROPAGATE(E);
  }

  if (maximized == already_maximized) {
    return Error_NONE;
  }

  if (maximized) {
    ShowWindow(window, SW_MAXIMIZE);
  } else {
    ShowWindow(window, SW_RESTORE);
  }

  return Error_NONE;
}

Error const*
IsMaximized(HWND window, bool* maximized) {
  assert(window != 0);

  WINDOWPLACEMENT placement;

  if (!GetWindowPlacement(window, &placement)) {
    Error_THROW(Error_Code_WINDOW, "Cannot get window placement.");
  }

  if (placement.showCmd == SW_MAXIMIZE) {
    *maximized = true;
  } else {
    *maximized = false;
  }

  return Error_NONE;
}

Error const*
ToggleMaximized(HWND window) {
  assert(window != 0);

  ERROR_DECLARATION;

  bool maximized;

  if ((E = IsMaximized(window, &maximized))) {
    Error_PROPAGATE(E);
  }

  return SetMaximized(window, !maximized);
}
// =============================================================================
// }}} Internal
// =============================================================================
// }}} Definitions
// =============================================================================
// }}} Functions

// Modeline {{{
// =============================================================================
// vim:ft=c:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
// =============================================================================
// }}} Modeline
