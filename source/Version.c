// Preamble {{{
// =============================================================================
//        File: Version.c
// -----------------------------------------------------------------------------
//     Version: 0.0.0
// -----------------------------------------------------------------------------
//     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
// -----------------------------------------------------------------------------
// Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
// -----------------------------------------------------------------------------
//  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
// -----------------------------------------------------------------------------
//     License: This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be useful,
//              but WITHOUT ANY WARRANTY; without even the implied warranty of
//              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//              General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program. If not, see
//              <http://www.gnu.org/licenses/>.
// =============================================================================
// }}} Preamble

// Preprocessor {{{
// =============================================================================
// Includes {{{
// =============================================================================
#include "Global"
// =============================================================================
// }}} Includes

// Definitions {{{
// =============================================================================
#define DEMANGLED Vim_MWM_DEMANGLED
#define PUBLIC    Vim_MWM_PUBLIC
#define UNUSED    Vim_MWM_UNUSED

#define Version_MAJOR 0
#define Version_MINOR 0
#define Version_PATCH 0

#define Version_STRING "0.0.0"
// =============================================================================
// }}} Definitions
// =============================================================================
// }}} Preprocessor

// Functions {{{
// =============================================================================
// Declarations {{{
// =============================================================================
// External {{{
// =============================================================================
DEMANGLED
long
__cdecl
PUBLIC
Vim_MWM_Version_GetMajor(long argument);

DEMANGLED
long
__cdecl
PUBLIC
Vim_MWM_Version_GetMinor(long argument);

DEMANGLED
long
__cdecl
PUBLIC
Vim_MWM_Version_GetPatch(long argument);

DEMANGLED
char const*
__cdecl
PUBLIC
Vim_MWM_Version_ToString(long argument);
// =============================================================================
// }}} External
// =============================================================================
// }}} Declarations

// Definitions {{{
// =============================================================================
// External {{{
// =============================================================================
long
Vim_MWM_Version_GetMajor(long argument) {
  UNUSED(argument);

  return Version_MAJOR;
}

long
Vim_MWM_Version_GetMinor(long argument) {
  UNUSED(argument);

  return Version_MINOR;
}

long
Vim_MWM_Version_GetPatch(long argument) {
  UNUSED(argument);

  return Version_PATCH;
}

char const*
Vim_MWM_Version_ToString(long argument) {
  UNUSED(argument);

  return Version_STRING;
}
// =============================================================================
// }}} External
// =============================================================================
// }}} Definitions
// =============================================================================
// }}} Functions

// Modeline {{{
// =============================================================================
// vim:ft=c:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
// =============================================================================
// }}} Modeline
