// Preamble {{{
// =============================================================================
//        File: Restore.c
// -----------------------------------------------------------------------------
//     Version: 0.0.0
// -----------------------------------------------------------------------------
//     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
// -----------------------------------------------------------------------------
// Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
// -----------------------------------------------------------------------------
//  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
// -----------------------------------------------------------------------------
//     License: This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be useful,
//              but WITHOUT ANY WARRANTY; without even the implied warranty of
//              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//              General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program. If not, see
//              <http://www.gnu.org/licenses/>.
// =============================================================================
// }}} Preamble

// Preprocessor {{{
// =============================================================================
// Includes {{{
// =============================================================================
#include "GetMainWindow"

#include <assert.h>
#include <stdbool.h>
// =============================================================================
// }}} Includes

// Definitions {{{
// =============================================================================
#define DEMANGLED         Vim_MWM_DEMANGLED
#define PUBLIC            Vim_MWM_PUBLIC
#define UNUSED            Vim_MWM_UNUSED
#define E                 Vim_MWM_ERROR
#define ERROR_DECLARATION Vim_MWM_ERROR_DECLARATION
#define TRACE             Vim_MWM_TRACE
#define TRACE_LENGTH      Vim_MWM_TRACE_LENGTH
#define TRACE_DECLARATION Vim_MWM_TRACE_DECLARATION

#define GetMainWindow Vim_MWM_GetMainWindow

#define Error_NONE   Vim_MWM_Error_NONE
#define Error_CREATE Vim_MWM_Error_CREATE
#define Error_THROW  Vim_MWM_Error_THROW

#define Error_ToTrace Vim_MWM_Error_ToTrace

#define Error_Code_WINDOW Vim_MWM_Error_Code_WINDOW
// =============================================================================
// }}} Definitions
// =============================================================================
// }}} Preprocessor

// Typedefs {{{
// =============================================================================
typedef Vim_MWM_Error Error;
// =============================================================================
// }}} Typedefs

// Functions {{{
// =============================================================================
// Declarations {{{
// =============================================================================
// External {{{
// =============================================================================
DEMANGLED
char const*
__cdecl
PUBLIC
Vim_MWM_Restore(long argument);

DEMANGLED
char const*
__cdecl
PUBLIC
Vim_MWM_IsRestored(long argument);
// =============================================================================
// }}} External

// Internal {{{
// =============================================================================
static
Error const*
Vim_MWM_PRIVATE
Restore(HWND window);

static
Error const*
Vim_MWM_PRIVATE
IsRestored(HWND window, bool* restored);
// =============================================================================
// }}} Internal
// =============================================================================
// }}} Declarations

// Definitions {{{
// =============================================================================
// External {{{
// =============================================================================
char const*
Vim_MWM_Restore(long argument) {
  UNUSED(argument);

  ERROR_DECLARATION;
  TRACE_DECLARATION;

  HWND main_window;

  if ((E = GetMainWindow(&main_window))) {
    Error_ToTrace(&Error_CREATE(E->code, E->message, E), TRACE, TRACE_LENGTH);

    return TRACE;
  }

  if ((E = Restore(main_window))) {
    Error_ToTrace(&Error_CREATE(E->code, "Cannot restore main window.", E),
                  TRACE,
                  TRACE_LENGTH);

    return TRACE;
  }

  return NULL;
}

char const*
Vim_MWM_IsRestored(long argument) {
  UNUSED(argument);

  ERROR_DECLARATION;
  TRACE_DECLARATION;

  HWND main_window;

  if ((E = GetMainWindow(&main_window))) {
    Error_ToTrace(&Error_CREATE(E->code, E->message, E), TRACE, TRACE_LENGTH);

    return TRACE;
  }

  bool restored;

  if ((E = IsRestored(main_window, &restored))) {
    Error_ToTrace(
      &Error_CREATE(E->code,
                    "Cannot determine whether main window is restored.",
                    E),
      TRACE,
      TRACE_LENGTH);

    return TRACE;
  }

  if (restored) {
    return Vim_MWM_TRUE;
  }

  return Vim_MWM_FALSE;
}
// =============================================================================
// }}} External

// Internal {{{
// =============================================================================
Error const*
Restore(HWND window) {
  assert(window != 0);

  ShowWindow(window, SW_RESTORE);

  return Error_NONE;
}

Error const*
IsRestored(HWND window, bool* restored) {
  assert(window != 0);

  WINDOWPLACEMENT placement;

  if (!GetWindowPlacement(window, &placement)) {
    Error_THROW(Error_Code_WINDOW, "Cannot get window placement.");
  }

  if (placement.showCmd == SW_SHOWNORMAL) {
    *restored = true;
  } else {
    *restored = false;
  }

  return Error_NONE;
}
// =============================================================================
// }}} Internal
// =============================================================================
// }}} Definitions
// =============================================================================
// }}} Functions

// Modeline {{{
// =============================================================================
// vim:ft=c:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
// =============================================================================
// }}} Modeline
