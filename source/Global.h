// Preamble {{{
// =============================================================================
//        File: Global.h
// -----------------------------------------------------------------------------
//     Version: 0.0.0
// -----------------------------------------------------------------------------
//     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
// -----------------------------------------------------------------------------
// Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
// -----------------------------------------------------------------------------
//  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
// -----------------------------------------------------------------------------
//     License: This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be useful,
//              but WITHOUT ANY WARRANTY; without even the implied warranty of
//              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//              General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program. If not, see
//              <http://www.gnu.org/licenses/>.
// =============================================================================
// }}} Preamble

#ifndef Vim_MWM_Global_h
#define Vim_MWM_Global_h

// Preprocessor {{{
// =============================================================================
// Definitions {{{
// =============================================================================
#define Vim_MWM_EXPORT __declspec(dllexport)
#define Vim_MWM_IMPORT __declspec(dllimport)
#define Vim_MWM_LOCAL

#ifdef Vim_MWM_EXPORTS
#  define Vim_MWM_PUBLIC Vim_MWM_EXPORT
#else
#  define Vim_MWM_PUBLIC Vim_MWM_IMPORT
#endif

#define Vim_MWM_PRIVATE Vim_MWM_LOCAL

#ifdef __cplusplus
#  define Vim_MWM_DEMANGLED extern "C"
#else
#  define Vim_MWM_DEMANGLED
#endif

#ifdef Vim_MWM_FORCE_SOURCE_NAME_ONLY
#  include <stdio.h>

#  define Vim_MWM_SOURCE_NAME                              \
  (strrchr(__FILE__, '/')  ? strrchr(__FILE__, '/')  + 1 : \
   strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : \
   __FILE__)
#else
#  define Vim_MWM_SOURCE_NAME __FILE__
#endif

#define Vim_MWM_LINE_NUMBER __LINE__

#define Vim_MWM_UNUSED(variable) \
  (void)(variable)

#define Vim_MWM_STRINGIFY(token) \
  # token

#define Vim_MWM_TO_STRING(token) \
  Vim_MWM_STRINGIFY(token)

#define Vim_MWM_FALSE "0"
#define Vim_MWM_TRUE  "1"

#define Vim_MWM_ERROR             Vim_MWM_error_
#define Vim_MWM_ERROR_DECLARATION Error const* Vim_MWM_ERROR

#define Vim_MWM_TRACE        Vim_MWM_trace_
#define Vim_MWM_TRACE_LENGTH 1024
#define Vim_MWM_TRACE_DECLARATION \
  static char Vim_MWM_TRACE[Vim_MWM_TRACE_LENGTH]
// =============================================================================
// }}} Definitions
// =============================================================================
// }}} Preprocessor

#endif // Vim_MWM_Global_h

// Modeline {{{
// =============================================================================
// vim:ft=c:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
// =============================================================================
// }}} Modeline
