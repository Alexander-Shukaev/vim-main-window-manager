// Preamble {{{
// =============================================================================
//        File: Error.c
// -----------------------------------------------------------------------------
//     Version: 0.0.0
// -----------------------------------------------------------------------------
//     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
// -----------------------------------------------------------------------------
// Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
// -----------------------------------------------------------------------------
//  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
// -----------------------------------------------------------------------------
//     License: This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be useful,
//              but WITHOUT ANY WARRANTY; without even the implied warranty of
//              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//              General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program. If not, see
//              <http://www.gnu.org/licenses/>.
// =============================================================================
// }}} Preamble

// Preprocessor {{{
// =============================================================================
// Includes {{{
// =============================================================================
#include "Error"

#include <assert.h>
#include <stdio.h>
// =============================================================================
// }}} Includes

// Definitions {{{
// =============================================================================
#define STRINGIFY Vim_MWM_STRINGIFY

#define Error_NONE Vim_MWM_Error_NONE
// =============================================================================
// }}} Definitions
// =============================================================================
// }}} Preprocessor

// Typedefs {{{
// =============================================================================
typedef Vim_MWM_Error_Code Error_Code;
// =============================================================================
// }}} Typedefs

// Variables {{{
// =============================================================================
// Definitions {{{
// =============================================================================
// Internal {{{
// =============================================================================
static
char const* const
Error_Code_identifiers[] = {
  STRINGIFY(Vim_MWM_Error_Code_NONE),
  STRINGIFY(Vim_MWM_Error_Code_ILLEGAL_ARGUMENT),
  STRINGIFY(Vim_MWM_Error_Code_WINDOW),
  STRINGIFY(Vim_MWM_Error_Code_MONITOR),
};

static
char const* const
Error_Code_summaries[] = {
  "",
  "Function has been passed illegal argument.",
  "General window related issue.",
  "General monitor related issue.",
};
// =============================================================================
// }}} Internal
// =============================================================================
// }}} Definitions
// =============================================================================
// }}} Variables

// Typedefs {{{
// =============================================================================
typedef Vim_MWM_Error Error;
// =============================================================================
// }}} Typedefs

// Functions {{{
// =============================================================================
// Definitions {{{
// =============================================================================
// External {{{
// =============================================================================
int
Vim_MWM_Error_ToString(Error const* error, char* string, size_t length) {
  return snprintf(string,
                  length,
                  "%s:%u: %s",
                  error->source_name,
                  error->line_number,
                  error->message);
}

int
Vim_MWM_Error_ToTrace(Error const* error, char* trace, size_t length) {
  int i = 0;
  int j = 0;

  while (error != Error_NONE) {
    j = Vim_MWM_Error_ToString(error, &trace[i], length);

    if (j < 0) {
      return j;
    }

    i += j;

    j = snprintf(&trace[i], length, "\n");

    if (j < 0) {
      return j;
    }

    i += j;

    error = error->cause;
  }

  return i;
}

char const*
Vim_MWM_Error_Code_GetIdentifier(Error_Code code) {
  assert(code >= 0 && code < Vim_MWM_Error_Code_GetCount());

  return Error_Code_identifiers[code];
}

char const*
Vim_MWM_Error_Code_GetSummary(Error_Code code) {
  assert(code >= 0 && code < Vim_MWM_Error_Code_GetCount());

  return Error_Code_summaries[code];
}

size_t
Vim_MWM_Error_Code_GetCount() {
  return sizeof (Error_Code_summaries) / sizeof (Error_Code_summaries[0]);
}
// =============================================================================
// }}} External
// =============================================================================
// }}} Definitions
// =============================================================================
// }}} Functions

// Modeline {{{
// =============================================================================
// vim:ft=c:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
// =============================================================================
// }}} Modeline
