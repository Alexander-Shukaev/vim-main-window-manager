// Preamble {{{
// =============================================================================
//        File: Error.h
// -----------------------------------------------------------------------------
//     Version: 0.0.0
// -----------------------------------------------------------------------------
//     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
// -----------------------------------------------------------------------------
// Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
// -----------------------------------------------------------------------------
//  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
// -----------------------------------------------------------------------------
//     License: This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be useful,
//              but WITHOUT ANY WARRANTY; without even the implied warranty of
//              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//              General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program. If not, see
//              <http://www.gnu.org/licenses/>.
// =============================================================================
// }}} Preamble

#ifndef Vim_MWM_Error_h
#define Vim_MWM_Error_h

// Preprocessor {{{
// =============================================================================
// Includes {{{
// =============================================================================
#include "Global"

#include <stddef.h>
// =============================================================================
// }}} Includes

// Definitions {{{
// =============================================================================
#define Vim_MWM_Error_NONE NULL

#define Vim_MWM_Error_CREATE(code, message, cause) \
  (Vim_MWM_Error){Vim_MWM_SOURCE_NAME,             \
                  Vim_MWM_LINE_NUMBER,             \
                  code,                            \
                  message,                         \
                  cause}

#define Vim_MWM_Error_THROW(code, message) \
  Vim_MWM_Error_CHAIN(code, message, Vim_MWM_Error_NONE)

#define Vim_MWM_Error_CHAIN(code, message, cause)                      \
  {                                                                    \
    static Vim_MWM_Error Vim_MWM_Error_error_;                         \
                                                                       \
    Vim_MWM_Error_error_ = Vim_MWM_Error_CREATE(code, message, cause); \
                                                                       \
    return &Vim_MWM_Error_error_;                                      \
  }

#define Vim_MWM_Error_RETHROW(error) \
  Vim_MWM_Error_RECHAIN(error, Vim_MWM_Error_NONE)

#define Vim_MWM_Error_RECHAIN(error, cause)         \
  Vim_MWM_Error_CHAIN(error == Vim_MWM_Error_NONE ? \
                      Vim_MWM_Error_Code_NONE     : \
                      error->code,                  \
                      error == Vim_MWM_Error_NONE ? \
                      Vim_MWM_Error_Code_NONE     : \
                      error->message,               \
                      cause)

#define Vim_MWM_Error_PROPAGATE(error) \
  Vim_MWM_Error_RECHAIN(error, error)
// =============================================================================
// }}} Definitions
// =============================================================================
// }}} Preprocessor

// Typedefs {{{
// =============================================================================
typedef struct Vim_MWM_Error Vim_MWM_Error;
typedef enum {
  Vim_MWM_Error_Code_NONE = 0,
  Vim_MWM_Error_Code_ILLEGAL_ARGUMENT,
  Vim_MWM_Error_Code_WINDOW,
  Vim_MWM_Error_Code_MONITOR,
} Vim_MWM_Error_Code;
// =============================================================================
// }}} Typedefs

// Types {{{
// =============================================================================
struct Vim_MWM_Error {
  char const*          source_name;
  unsigned int         line_number;
  Vim_MWM_Error_Code   code;
  char const*          message;
  Vim_MWM_Error const* cause;
};
// =============================================================================
// }}} Types

// Functions {{{
// =============================================================================
// Declarations {{{
// =============================================================================
// External {{{
// =============================================================================
int
Vim_MWM_PRIVATE
Vim_MWM_Error_ToString(Vim_MWM_Error const* error, char* string, size_t length);

int
Vim_MWM_PRIVATE
Vim_MWM_Error_ToTrace(Vim_MWM_Error const* error, char* trace, size_t length);

char const*
Vim_MWM_PRIVATE
Vim_MWM_Error_Code_GetIdentifier(Vim_MWM_Error_Code code);

char const*
Vim_MWM_PRIVATE
Vim_MWM_Error_Code_GetSummary(Vim_MWM_Error_Code code);

size_t
Vim_MWM_PRIVATE
Vim_MWM_Error_Code_GetCount();
// =============================================================================
// }}} External
// =============================================================================
// }}} Declarations
// =============================================================================
// }}} Functions

#endif // Vim_MWM_Error_h

// Modeline {{{
// =============================================================================
// vim:ft=c:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
// =============================================================================
// }}} Modeline
