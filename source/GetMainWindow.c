// Preamble {{{
// =============================================================================
//        File: GetMainWindow.c
// -----------------------------------------------------------------------------
//     Version: 0.0.0
// -----------------------------------------------------------------------------
//     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
// -----------------------------------------------------------------------------
// Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
// -----------------------------------------------------------------------------
//  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
// -----------------------------------------------------------------------------
//     License: This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be useful,
//              but WITHOUT ANY WARRANTY; without even the implied warranty of
//              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//              General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program. If not, see
//              <http://www.gnu.org/licenses/>.
// =============================================================================
// }}} Preamble

// Preprocessor {{{
// =============================================================================
// Includes {{{
// =============================================================================
#include "GetMainWindow"

#include <string.h>
// =============================================================================
// }}} Includes

// Definitions {{{
// =============================================================================
#define PRIVATE Vim_MWM_PRIVATE

#define Error_NONE  Vim_MWM_Error_NONE
#define Error_THROW Vim_MWM_Error_THROW

#define Error_Code_WINDOW Vim_MWM_Error_Code_WINDOW
// =============================================================================
// }}} Definitions
// =============================================================================
// }}} Preprocessor

// Typedefs {{{
// =============================================================================
typedef Vim_MWM_Error Error;
// =============================================================================
// }}} Typedefs

// Functions {{{
// =============================================================================
// Declarations {{{
// =============================================================================
// Internal {{{
// =============================================================================
static
BOOL
CALLBACK
PRIVATE
FindMainWindow(HWND window, LPARAM l_param);
// =============================================================================
// }}} Internal
// =============================================================================
// }}} Declarations

// Definitions {{{
// =============================================================================
// External {{{
// =============================================================================
Error const*
Vim_MWM_GetMainWindow(HWND* main_window) {
  EnumThreadWindows(GetCurrentThreadId(), FindMainWindow, (LPARAM)main_window);

  if (*main_window) {
    return Error_NONE;
  }

  Error_THROW(Error_Code_WINDOW, "Cannot find main window.");
}
// =============================================================================
// }}} External

// Internal {{{
// =============================================================================
BOOL
CALLBACK
FindMainWindow(HWND window, LPARAM l_param) {
  HWND* main_window = (HWND*)l_param;

  *main_window = 0;

  if (GetParent(window)) {
    return TRUE;
  }

  char window_class_name[4];

  if (!GetClassName(window, window_class_name, 4)) {
    return TRUE;
  }

  if (strcmp(window_class_name, "Vim")) {
    return TRUE;
  }

  *main_window = window;

  return FALSE;
}
// =============================================================================
// }}} Internal
// =============================================================================
// }}} Definitions
// =============================================================================
// }}} Functions

// Modeline {{{
// =============================================================================
// vim:ft=c:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
// =============================================================================
// }}} Modeline
