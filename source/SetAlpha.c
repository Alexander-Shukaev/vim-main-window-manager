// Preamble {{{
// =============================================================================
//        File: SetAlpha.c
// -----------------------------------------------------------------------------
//     Version: 0.0.0
// -----------------------------------------------------------------------------
//     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
// -----------------------------------------------------------------------------
// Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
// -----------------------------------------------------------------------------
//  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
// -----------------------------------------------------------------------------
//     License: This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be useful,
//              but WITHOUT ANY WARRANTY; without even the implied warranty of
//              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//              General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program. If not, see
//              <http://www.gnu.org/licenses/>.
// =============================================================================
// }}} Preamble

// Preprocessor {{{
// =============================================================================
// Includes {{{
// =============================================================================
#include "GetMainWindow"

#include <stdio.h>
// =============================================================================
// }}} Includes

// Definitions {{{
// =============================================================================
#define DEMANGLED         Vim_MWM_DEMANGLED
#define PUBLIC            Vim_MWM_PUBLIC
#define UNUSED            Vim_MWM_UNUSED
#define E                 Vim_MWM_ERROR
#define ERROR_DECLARATION Vim_MWM_ERROR_DECLARATION
#define TRACE             Vim_MWM_TRACE
#define TRACE_LENGTH      Vim_MWM_TRACE_LENGTH
#define TRACE_DECLARATION Vim_MWM_TRACE_DECLARATION

#define GetMainWindow Vim_MWM_GetMainWindow

#define Error_NONE   Vim_MWM_Error_NONE
#define Error_CREATE Vim_MWM_Error_CREATE

#define Error_ToTrace Vim_MWM_Error_ToTrace

#define Error_Code_ILLEGAL_ARGUMENT Vim_MWM_Error_Code_ILLEGAL_ARGUMENT
#define Error_Code_WINDOW           Vim_MWM_Error_Code_WINDOW
// =============================================================================
// }}} Definitions
// =============================================================================
// }}} Preprocessor

// Typedefs {{{
// =============================================================================
typedef Vim_MWM_Error Error;
// =============================================================================
// }}} Typedefs

// Functions {{{
// =============================================================================
// Declarations {{{
// =============================================================================
// External {{{
// =============================================================================
DEMANGLED
char const*
__cdecl
PUBLIC
Vim_MWM_SetAlpha(long alpha);

DEMANGLED
char const*
__cdecl
PUBLIC
Vim_MWM_GetAlpha(long argument);
// =============================================================================
// }}} External
// =============================================================================
// }}} Declarations

// Definitions {{{
// =============================================================================
// External {{{
// =============================================================================
char const*
Vim_MWM_SetAlpha(long alpha) {
  ERROR_DECLARATION;
  TRACE_DECLARATION;

  if (alpha < 0 || alpha > 255) {
    Error_ToTrace(&Error_CREATE(Error_Code_ILLEGAL_ARGUMENT,
                                "For `alpha`, only values between \"0\" and"
                                " "
                                "\"255\" make sense.",
                                Error_NONE),
                  TRACE,
                  TRACE_LENGTH);

    return TRACE;
  }

  HWND main_window;

  if ((E = GetMainWindow(&main_window))) {
    Error_ToTrace(&Error_CREATE(E->code, E->message, E), TRACE, TRACE_LENGTH);

    return TRACE;
  }

  LONG_PTR ex_style = GetWindowLongPtr(main_window, GWL_EXSTYLE);

  if (!ex_style) {
    Error_ToTrace(&Error_CREATE(Error_Code_WINDOW,
                                "Cannot get main window extended style.",
                                Error_NONE),
                  TRACE,
                  TRACE_LENGTH);

    return TRACE;
  }

  if (alpha == 255) {
    ex_style &= ~WS_EX_LAYERED;
  } else {
    ex_style |=  WS_EX_LAYERED;
  }

  SetLastError(0);

  if (!SetWindowLongPtr(main_window, GWL_EXSTYLE, ex_style) && GetLastError()) {
    Error_ToTrace(&Error_CREATE(Error_Code_WINDOW,
                                "Cannot set main window extended style.",
                                Error_NONE),
                  TRACE,
                  TRACE_LENGTH);

    return TRACE;
  }

  if (alpha == 255) {
    return NULL;
  }

  if (!SetLayeredWindowAttributes(main_window, 0, (BYTE)alpha, LWA_ALPHA)) {
    Error_ToTrace(&Error_CREATE(Error_Code_WINDOW,
                                "Cannot set main window attribute.",
                                Error_NONE),
                  TRACE,
                  TRACE_LENGTH);

    return TRACE;
  }

  return NULL;
}

char const*
Vim_MWM_GetAlpha(long argument) {
  UNUSED(argument);

  ERROR_DECLARATION;
  TRACE_DECLARATION;

  HWND main_window;

  if ((E = GetMainWindow(&main_window))) {
    Error_ToTrace(&Error_CREATE(E->code, E->message, E), TRACE, TRACE_LENGTH);

    return TRACE;
  }

  LONG_PTR ex_style = GetWindowLongPtr(main_window, GWL_EXSTYLE);

  if (!ex_style) {
    Error_ToTrace(&Error_CREATE(Error_Code_WINDOW,
                                "Cannot get main window extended style.",
                                Error_NONE),
                  TRACE,
                  TRACE_LENGTH);

    return TRACE;
  }

  if (!(ex_style & WS_EX_LAYERED)) {
    return "255";
  }

  BYTE alpha;

  if (!GetLayeredWindowAttributes(main_window, NULL, &alpha, NULL)) {
    Error_ToTrace(&Error_CREATE(Error_Code_WINDOW,
                                "Cannot get main window attribute.",
                                Error_NONE),
                  TRACE,
                  TRACE_LENGTH);

    return TRACE;
  }

  snprintf(TRACE, 4, "%u", alpha);

  return TRACE;
}
// =============================================================================
// }}} External
// =============================================================================
// }}} Definitions
// =============================================================================
// }}} Functions

// Modeline {{{
// =============================================================================
// vim:ft=c:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
// =============================================================================
// }}} Modeline
