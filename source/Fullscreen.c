// Preamble {{{
// =============================================================================
//        File: Fullscreen.c
// -----------------------------------------------------------------------------
//     Version: 0.0.0
// -----------------------------------------------------------------------------
//     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
// -----------------------------------------------------------------------------
// Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
// -----------------------------------------------------------------------------
//  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
// -----------------------------------------------------------------------------
//     License: This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be useful,
//              but WITHOUT ANY WARRANTY; without even the implied warranty of
//              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//              General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program. If not, see
//              <http://www.gnu.org/licenses/>.
// =============================================================================
// }}} Preamble

// Preprocessor {{{
// =============================================================================
// Includes {{{
// =============================================================================
#include "GetMainWindow"

#include <assert.h>
#include <stdbool.h>
// =============================================================================
// }}} Includes

// Definitions {{{
// =============================================================================
#define DEMANGLED         Vim_MWM_DEMANGLED
#define PUBLIC            Vim_MWM_PUBLIC
#define PRIVATE           Vim_MWM_PRIVATE
#define UNUSED            Vim_MWM_UNUSED
#define E                 Vim_MWM_ERROR
#define ERROR_DECLARATION Vim_MWM_ERROR_DECLARATION
#define TRACE             Vim_MWM_TRACE
#define TRACE_LENGTH      Vim_MWM_TRACE_LENGTH
#define TRACE_DECLARATION Vim_MWM_TRACE_DECLARATION

#define GetMainWindow Vim_MWM_GetMainWindow

#define Error_NONE      Vim_MWM_Error_NONE
#define Error_CREATE    Vim_MWM_Error_CREATE
#define Error_THROW     Vim_MWM_Error_THROW
#define Error_PROPAGATE Vim_MWM_Error_PROPAGATE

#define Error_ToTrace Vim_MWM_Error_ToTrace

#define Error_Code_WINDOW  Vim_MWM_Error_Code_WINDOW
#define Error_Code_MONITOR Vim_MWM_Error_Code_MONITOR
// =============================================================================
// }}} Definitions
// =============================================================================
// }}} Preprocessor

// Typedefs {{{
// =============================================================================
typedef Vim_MWM_Error Error;
// =============================================================================
// }}} Typedefs

// Functions {{{
// =============================================================================
// Declarations {{{
// =============================================================================
// External {{{
// =============================================================================
DEMANGLED
char const*
__cdecl
PUBLIC
Vim_MWM_Fullscreen(long argument);

DEMANGLED
char const*
__cdecl
PUBLIC
Vim_MWM_SetFullscreen(long fullscreen);

DEMANGLED
char const*
__cdecl
PUBLIC
Vim_MWM_IsFullscreen(long argument);

DEMANGLED
char const*
__cdecl
PUBLIC
Vim_MWM_ToggleFullscreen(long argument);
// =============================================================================
// }}} External

// Internal {{{
// =============================================================================
static
Error const*
PRIVATE
SetFullscreen(HWND window, bool fullscreen);

static
Error const*
PRIVATE
IsFullscreen(HWND window, bool* fullscreen);

static
Error const*
PRIVATE
ToggleFullscreen(HWND window);
// =============================================================================
// }}} Internal
// =============================================================================
// }}} Declarations

// Definitions {{{
// =============================================================================
// External {{{
// =============================================================================
char const*
Vim_MWM_Fullscreen(long argument) {
  UNUSED(argument);

  return Vim_MWM_SetFullscreen(1);
}

char const*
Vim_MWM_SetFullscreen(long fullscreen) {
  ERROR_DECLARATION;
  TRACE_DECLARATION;

  HWND main_window;

  if ((E = GetMainWindow(&main_window))) {
    Error_ToTrace(&Error_CREATE(E->code, E->message, E), TRACE, TRACE_LENGTH);

    return TRACE;
  }

  if ((E = SetFullscreen(main_window, fullscreen))) {
    Error_ToTrace(&Error_CREATE(E->code,
                                "Cannot set fullscreen for main window.",
                                E),
                  TRACE,
                  TRACE_LENGTH);

    return TRACE;
  }

  return NULL;
}

char const*
Vim_MWM_IsFullscreen(long argument) {
  UNUSED(argument);

  ERROR_DECLARATION;
  TRACE_DECLARATION;

  HWND main_window;

  if ((E = GetMainWindow(&main_window))) {
    Error_ToTrace(&Error_CREATE(E->code, E->message, E), TRACE, TRACE_LENGTH);

    return TRACE;
  }

  bool fullscreen;

  if ((E = IsFullscreen(main_window, &fullscreen))) {
    Error_ToTrace(
      &Error_CREATE(E->code,
                    "Cannot determine whether main window is fullscreen.",
                    E),
      TRACE,
      TRACE_LENGTH);

    return TRACE;
  }

  if (fullscreen) {
    return Vim_MWM_TRUE;
  }

  return Vim_MWM_FALSE;
}

char const*
Vim_MWM_ToggleFullscreen(long argument) {
  UNUSED(argument);

  ERROR_DECLARATION;
  TRACE_DECLARATION;

  HWND main_window;

  if ((E = GetMainWindow(&main_window))) {
    Error_ToTrace(&Error_CREATE(E->code, E->message, E), TRACE, TRACE_LENGTH);

    return TRACE;
  }

  if ((E = ToggleFullscreen(main_window))) {
    Error_ToTrace(&Error_CREATE(E->code,
                                "Cannot toggle fullscreen for main window.",
                                E),
                  TRACE,
                  TRACE_LENGTH);

    return TRACE;
  }

  return NULL;
}
// =============================================================================
// }}} External

// Internal {{{
// =============================================================================
Error const*
SetFullscreen(HWND window, bool fullscreen) {
  assert(window != 0);

  ERROR_DECLARATION;

  bool already_fullscreen;

  if ((E = IsFullscreen(window, &already_fullscreen))) {
    Error_PROPAGATE(E);
  }

  if (fullscreen == already_fullscreen) {
    return Error_NONE;
  }

  LONG_PTR style = GetWindowLongPtr(window, GWL_STYLE);

  if (!style) {
    Error_THROW(Error_Code_WINDOW, "Cannot get window style.");
  }

  if (fullscreen) {
    style &= ~(WS_CAPTION | WS_THICKFRAME);
  } else {
    style |=   WS_CAPTION | WS_THICKFRAME;
  }

  SetLastError(0);

  if (!SetWindowLongPtr(window, GWL_STYLE, style) && GetLastError()) {
    Error_THROW(Error_Code_WINDOW, "Cannot set window style.");
  }

  if (fullscreen) {
    HMONITOR monitor = MonitorFromWindow(window, MONITOR_DEFAULTTONEAREST);

    if (!monitor) {
      Error_THROW(Error_Code_MONITOR, "Cannot find window monitor.");
    }

    MONITORINFO info = {.cbSize = sizeof (MONITORINFO)};

    if (!GetMonitorInfo(monitor, &info)) {
      Error_THROW(Error_Code_MONITOR, "Cannot get monitor information.");
    }

    ShowWindow(window, SW_MAXIMIZE);

    if (!SetWindowPos(window,
                      HWND_TOP,
                      info.rcMonitor.left,
                      info.rcMonitor.top,
                      info.rcMonitor.right - info.rcMonitor.left,
                      info.rcMonitor.bottom - info.rcMonitor.top,
                      SWP_SHOWWINDOW)) {
      Error_THROW(Error_Code_WINDOW, "Cannot set window position.");
    }
  } else {
    if (!SetWindowPos(window,
                      0,
                      0,
                      0,
                      0,
                      0,
                      SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED)) {
      Error_THROW(Error_Code_WINDOW, "Cannot set window position.");
    }
  }

  return Error_NONE;
}

Error const*
IsFullscreen(HWND window, bool* fullscreen) {
  assert(window != 0);

  LONG_PTR style = GetWindowLongPtr(window, GWL_STYLE);

  if (!style) {
    Error_THROW(Error_Code_WINDOW, "Cannot get window style.");
  }

  RECT rect;

  if (!GetWindowRect(window, &rect)) {
    Error_THROW(Error_Code_WINDOW, "Cannot get window rectangle.");
  }

  HMONITOR monitor = MonitorFromWindow(window, MONITOR_DEFAULTTONEAREST);

  if (!monitor) {
    Error_THROW(Error_Code_MONITOR, "Cannot find window monitor.");
  }

  MONITORINFO info = {.cbSize = sizeof (MONITORINFO)};

  if (!GetMonitorInfo(monitor, &info)) {
    Error_THROW(Error_Code_MONITOR, "Cannot get monitor information.");
  }

  if (!(style & (WS_CAPTION | WS_THICKFRAME))
      &&
      rect.left   == info.rcMonitor.left
      &&
      rect.right  == info.rcMonitor.right
      &&
      rect.top    == info.rcMonitor.top
      &&
      rect.bottom == info.rcMonitor.bottom) {
    *fullscreen = true;
  } else {
    *fullscreen = false;
  }

  return Error_NONE;
}

Error const*
ToggleFullscreen(HWND window) {
  assert(window != 0);

  ERROR_DECLARATION;

  bool fullscreen;

  if ((E = IsFullscreen(window, &fullscreen))) {
    Error_PROPAGATE(E);
  }

  return SetFullscreen(window, !fullscreen);
}
// =============================================================================
// }}} Internal
// =============================================================================
// }}} Definitions
// =============================================================================
// }}} Functions

// Modeline {{{
// =============================================================================
// vim:ft=c:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
// =============================================================================
// }}} Modeline
