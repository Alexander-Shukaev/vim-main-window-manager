" Preamble {{{
" ==============================================================================
"        File: MWM.vim
" ------------------------------------------------------------------------------
"     Version: 0.0.0
" ------------------------------------------------------------------------------
"     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
" Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"     License: This program is free software: you can redistribute it and/or
"              modify it under the terms of the GNU General Public License as
"              published by the Free Software Foundation, either version 3 of
"              the License, or (at your option) any later version.
"
"              This program is distributed in the hope that it will be useful,
"              but WITHOUT ANY WARRANTY; without even the implied warranty of
"              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
"              General Public License for more details.
"
"              You should have received a copy of the GNU General Public License
"              along with this program. If not, see
"              <http://www.gnu.org/licenses/>.
" ==============================================================================
" }}} Preamble

if exists('g:MWM#autoload')
  finish
endif

if !has('gui_running')
  finish
endif

if !has('win32') && !has('win32unix')
  finish
endif

let g:MWM#autoload = 1

let s:cpoptions = &cpoptions
set cpoptions&vim

" Functions {{{
" ==============================================================================
" Public {{{
" ==============================================================================
function! MWM#Fullscreen()
  if MWM#IsReady()
    try
      let l:result = MWM#Library#Call('Vim_MWM_Fullscreen', 0)
    endtry
    if !empty(l:result)
      throw s:Join(l:result)
    endif
  else
    augroup MWM#Fullscreen
      autocmd!
      autocmd VimEnter *
      \ try                                                                    |
      \   call MWM#Fullscreen()                                                |
      \ catch                                                                  |
      \   echohl ErrorMsg                                                      |
      \   echomsg
      \     "MWM: Error:"                                                      .
      \     ' '                                                                .
      \     v:exception                                                        |
      \   echohl None                                                          |
      \ endtry
    augroup END
  endif
endfunction
" ------------------------------------------------------------------------------
function! MWM#SetFullscreen(fullscreen)
  if     type(a:fullscreen) == type(0)
    let s:fullscreen = a:fullscreen
  elseif type(a:fullscreen) == type('')
    let s:fullscreen = str2nr(a:fullscreen)
  else
    throw
    \ "`a:fullscreen` should be one of types {`Number`, `String`}, and only"
    \ ' '
    \ "values \"0\" and \"1\" make sense."
  endif
  if MWM#IsReady()
    try
      let l:result = MWM#Library#Call('Vim_MWM_SetFullscreen', s:fullscreen)
    endtry
    if !empty(l:result)
      throw s:Join(l:result)
    endif
  else
    augroup MWM#SetFullscreen
      autocmd!
      autocmd VimEnter *
      \ try                                                                    |
      \   call MWM#SetFullscreen(s:fullscreen)                                 |
      \ catch                                                                  |
      \   echohl ErrorMsg                                                      |
      \   echomsg
      \     "MWM: Error:"                                                      .
      \     ' '                                                                .
      \     v:exception                                                        |
      \   echohl None                                                          |
      \ endtry
    augroup END
  endif
endfunction
" ------------------------------------------------------------------------------
function! MWM#IsFullscreen()
  let l:result = MWM#Library#Call('Vim_MWM_IsFullscreen', 0)
  if l:result == string(str2nr(l:result))
    return str2nr(l:result)
  endif
  throw s:Join(l:result)
endfunction
" ------------------------------------------------------------------------------
function! MWM#ToggleFullscreen()
  if MWM#IsReady()
    try
      let l:result = MWM#Library#Call('Vim_MWM_ToggleFullscreen', 0)
    endtry
    if !empty(l:result)
      throw s:Join(l:result)
    endif
  else
    augroup MWM#ToggleFullscreen
      autocmd!
      autocmd VimEnter *
      \ try                                                                    |
      \   call MWM#ToggleFullscreen()                                          |
      \ catch                                                                  |
      \   echohl ErrorMsg                                                      |
      \   echomsg
      \     "MWM: Error:"                                                      .
      \     ' '                                                                .
      \     v:exception                                                        |
      \   echohl None                                                          |
      \ endtry
    augroup END
  endif
endfunction
" ------------------------------------------------------------------------------
function! MWM#Maximize()
  if MWM#IsReady()
    try
      let l:result = MWM#Library#Call('Vim_MWM_Maximize', 0)
    endtry
    if !empty(l:result)
      throw s:Join(l:result)
    endif
  else
    augroup MWM#Maximize
      autocmd!
      autocmd VimEnter *
      \ try                                                                    |
      \   call MWM#Maximize()                                                  |
      \ catch                                                                  |
      \   echohl ErrorMsg                                                      |
      \   echomsg
      \     "MWM: Error:"                                                      .
      \     ' '                                                                .
      \     v:exception                                                        |
      \   echohl None                                                          |
      \ endtry
    augroup END
  endif
endfunction
" ------------------------------------------------------------------------------
function! MWM#SetMaximized(maximized)
  if     type(a:maximized) == type(0)
    let s:maximized = a:maximized
  elseif type(a:maximized) == type('')
    let s:maximized = str2nr(a:maximized)
  else
    throw
    \ "`a:maximized` should be one of types {`Number`, `String`}, and only"
    \ ' '
    \ "values \"0\" and \"1\" make sense."
  endif
  if MWM#IsReady()
    try
      let l:result = MWM#Library#Call('Vim_MWM_SetMaximized', s:maximized)
    endtry
    if !empty(l:result)
      throw s:Join(l:result)
    endif
  else
    augroup MWM#SetMaximized
      autocmd!
      autocmd VimEnter *
      \ try                                                                    |
      \   call MWM#SetMaximized(s:maximized)                                   |
      \ catch                                                                  |
      \   echohl ErrorMsg                                                      |
      \   echomsg
      \     "MWM: Error:"                                                      .
      \     ' '                                                                .
      \     v:exception                                                        |
      \   echohl None                                                          |
      \ endtry
    augroup END
  endif
endfunction
" ------------------------------------------------------------------------------
function! MWM#IsMaximized()
  let l:result = MWM#Library#Call('Vim_MWM_IsMaximized', 0)
  if l:result == string(str2nr(l:result))
    return str2nr(l:result)
  endif
  throw s:Join(l:result)
endfunction
" ------------------------------------------------------------------------------
function! MWM#ToggleMaximized()
  if MWM#IsReady()
    try
      let l:result = MWM#Library#Call('Vim_MWM_ToggleMaximized', 0)
    endtry
    if !empty(l:result)
      throw s:Join(l:result)
    endif
  else
    augroup MWM#ToggleMaximized
      autocmd!
      autocmd VimEnter *
      \ try                                                                    |
      \   call MWM#ToggleMaximized()                                           |
      \ catch                                                                  |
      \   echohl ErrorMsg                                                      |
      \   echomsg
      \     "MWM: Error:"                                                      .
      \     ' '                                                                .
      \     v:exception                                                        |
      \   echohl None                                                          |
      \ endtry
    augroup END
  endif
endfunction
" ------------------------------------------------------------------------------
function! MWM#Restore()
  if MWM#IsReady()
    try
      let l:result = MWM#Library#Call('Vim_MWM_Restore', 0)
    endtry
    if !empty(l:result)
      throw s:Join(l:result)
    endif
  else
    augroup MWM#Restore
      autocmd!
      autocmd VimEnter *
      \ try                                                                    |
      \   call MWM#Restore()                                                   |
      \ catch                                                                  |
      \   echohl ErrorMsg                                                      |
      \   echomsg
      \     "MWM: Error:"                                                      .
      \     ' '                                                                .
      \     v:exception                                                        |
      \   echohl None                                                          |
      \ endtry
    augroup END
  endif
endfunction
" ------------------------------------------------------------------------------
function! MWM#IsRestored()
  let l:result = MWM#Library#Call('Vim_MWM_IsRestored', 0)
  if l:result == string(str2nr(l:result))
    return str2nr(l:result)
  endif
  throw s:Join(l:result)
endfunction
" ------------------------------------------------------------------------------
function! MWM#SetAlpha(alpha)
  if     type(a:alpha) == type(0.0)
    let s:alpha = a:alpha
  elseif type(a:alpha) == type(0)
    let s:alpha = str2float(a:alpha)
  elseif type(a:alpha) == type('')
    let s:alpha = str2float(a:alpha)
  else
    throw
    \ "`a:alpha` should be one of types {`Float`, `Number`, `String`}, and only"
    \ ' '
    \ "values between \"0.0\" and \"1.0\" make sense."
  endif
  let s:alpha = s:Clamp(s:alpha, 0.0, 1.0)
  if MWM#IsReady()
    try
      let l:result = MWM#Library#Call('Vim_MWM_SetAlpha',
      \                               float2nr(round(s:alpha * 255)))
    endtry
    if !empty(l:result)
      throw s:Join(l:result)
    endif
  else
    augroup MWM#SetAlpha
      autocmd!
      autocmd VimEnter *
      \ try                                                                    |
      \   call MWM#SetAlpha(s:alpha)                                           |
      \ catch                                                                  |
      \   echohl ErrorMsg                                                      |
      \   echomsg
      \     "MWM: Error:"                                                      .
      \     ' '                                                                .
      \     v:exception                                                        |
      \   echohl None                                                          |
      \ endtry
    augroup END
  endif
endfunction
" ------------------------------------------------------------------------------
function! MWM#GetAlpha()
  let l:result = MWM#Library#Call('Vim_MWM_GetAlpha', 0)
  if l:result == string(str2nr(l:result))
    return s:Clamp(str2float(l:result) / 255, 0.0, 1.0)
  endif
  throw s:Join(l:result)
endfunction
" ------------------------------------------------------------------------------
function! MWM#GetID()
  return v:windowid
endfunction
" ------------------------------------------------------------------------------
function! MWM#IsReady()
  return MWM#GetID() != 0
endfunction
" ==============================================================================
" }}} Public

" Private {{{
" ==============================================================================
function! s:Clamp(value, min, max)
  if a:value < a:min
    return a:min
  elseif a:value > a:max
    return a:max
  else
    return a:value
  endif
endfunction
" ------------------------------------------------------------------------------
function! s:Join(string)
  return join(split(a:string, '\n'), ' ')
endfunction
" ==============================================================================
" }}} Private
" ==============================================================================
" }}} Functions

let &cpoptions = s:cpoptions
unlet s:cpoptions

" Modeline {{{
" ==============================================================================
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ==============================================================================
" }}} Modeline
