" Preamble {{{
" ==============================================================================
"        File: Version.vim
" ------------------------------------------------------------------------------
"     Version: 0.0.0
" ------------------------------------------------------------------------------
"     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
" Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"     License: This program is free software: you can redistribute it and/or
"              modify it under the terms of the GNU General Public License as
"              published by the Free Software Foundation, either version 3 of
"              the License, or (at your option) any later version.
"
"              This program is distributed in the hope that it will be useful,
"              but WITHOUT ANY WARRANTY; without even the implied warranty of
"              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
"              General Public License for more details.
"
"              You should have received a copy of the GNU General Public License
"              along with this program. If not, see
"              <http://www.gnu.org/licenses/>.
" ==============================================================================
" }}} Preamble

if exists('g:MWM#Version#autoload')
  finish
endif

let g:MWM#Version#autoload = 1

let s:cpoptions = &cpoptions
set cpoptions&vim

" Functions {{{
" ==============================================================================
" Public {{{
" ==============================================================================
function! MWM#Version#GetMajor()
  return s:major
endfunction
" ------------------------------------------------------------------------------
function! MWM#Version#GetMinor()
  return s:minor
endfunction
" ------------------------------------------------------------------------------
function! MWM#Version#GetPatch()
  return s:patch
endfunction
" ------------------------------------------------------------------------------
function! MWM#Version#ToString()
  return s:major . '.' . s:minor . '.' . s:patch
endfunction
" ==============================================================================
" }}} Public
" ==============================================================================
" }}} Functions

" Defaults {{{
" ==============================================================================
let s:major = 0
let s:minor = 0
let s:patch = 0
" ==============================================================================
" }}} Defaults

let &cpoptions = s:cpoptions
unlet s:cpoptions

" Modeline {{{
" ==============================================================================
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ==============================================================================
" }}} Modeline
