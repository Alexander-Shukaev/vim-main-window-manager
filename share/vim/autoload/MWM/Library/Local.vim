" Preamble {{{
" ==============================================================================
"        File: Local.vim
" ------------------------------------------------------------------------------
"     Version: 0.0.0
" ------------------------------------------------------------------------------
"     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
" Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"     License: This program is free software: you can redistribute it and/or
"              modify it under the terms of the GNU General Public License as
"              published by the Free Software Foundation, either version 3 of
"              the License, or (at your option) any later version.
"
"              This program is distributed in the hope that it will be useful,
"              but WITHOUT ANY WARRANTY; without even the implied warranty of
"              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
"              General Public License for more details.
"
"              You should have received a copy of the GNU General Public License
"              along with this program. If not, see
"              <http://www.gnu.org/licenses/>.
" ==============================================================================
" }}} Preamble

if exists('g:MWM#Library#Local#autoload')
  finish
endif

if !has('gui_running')
  finish
endif

if !has('win32') && !has('win32unix')
  finish
endif

let g:MWM#Library#Local#autoload = 1

let s:cpoptions = &cpoptions
set cpoptions&vim

" Functions {{{
" ==============================================================================
" Public {{{
" ==============================================================================
function! MWM#Library#Local#GetPath()
  return simplify(expand(s:path, 1))
endfunction
" ------------------------------------------------------------------------------
function! MWM#Library#Local#GetAbsolutePath()
  return fnamemodify(MWM#Library#Local#GetPath(), ':p')
endfunction
" ------------------------------------------------------------------------------
function! MWM#Library#Local#IsAbsolute()
  return
  \ MWM#Library#Local#GetPath()
  \ ==
  \ MWM#Library#Local#GetAbsolutePath()
endfunction
" ------------------------------------------------------------------------------
function! MWM#Library#Local#Exists()
  try
    return
    \ libcall(
    \   MWM#Library#Local#GetPath(),
    \   'Vim_MWM_Version_ToString',
    \   0
    \ )
    \ ==
    \ MWM#Version#ToString()
  catch
  endtry
  return 0
endfunction
" ------------------------------------------------------------------------------
function! MWM#Library#Local#Call(function, argument)
  if MWM#Library#Local#Exists()
    try
      return libcall(MWM#Library#Local#GetPath(), a:function, a:argument)
    catch
      throw
      \ "Cannot call function:"                                                .
      \ ' '                                                                    .
      \ '`'                                                                    .
      \ a:function                                                             .
      \ '('                                                                    .
      \ a:argument                                                             .
      \ ')'                                                                    .
      \ '`'                                                                    .
      \ ' '                                                                    .
      \ "from local library:"                                                  .
      \ ' '                                                                    .
      \ '"'                                                                    .
      \ MWM#Library#Local#GetPath()                                            .
      \ '"'                                                                    .
      \ '.'
    endtry
  else
    throw
    \ "Cannot find local library:"                                             .
    \ ' '                                                                      .
    \ '"'                                                                      .
    \ MWM#Library#Local#GetPath()                                              .
    \ '"'                                                                      .
    \ '.'
  endif
endfunction
" ==============================================================================
" }}} Public
" ==============================================================================
" }}} Functions

" Defaults {{{
" ==============================================================================
let s:path =
\ expand('<sfile>:p:h:h:h:h')                                                  .
\ '/'                                                                          .
\ 'bin'                                                                        .
\ '/'                                                                          .
\ 'vim-mwm'
" ==============================================================================
" }}} Defaults

let &cpoptions = s:cpoptions
unlet s:cpoptions

" Modeline {{{
" ==============================================================================
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ==============================================================================
" }}} Modeline
