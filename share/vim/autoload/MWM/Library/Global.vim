" Preamble {{{
" ==============================================================================
"        File: Global.vim
" ------------------------------------------------------------------------------
"     Version: 0.0.0
" ------------------------------------------------------------------------------
"     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
" Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"     License: This program is free software: you can redistribute it and/or
"              modify it under the terms of the GNU General Public License as
"              published by the Free Software Foundation, either version 3 of
"              the License, or (at your option) any later version.
"
"              This program is distributed in the hope that it will be useful,
"              but WITHOUT ANY WARRANTY; without even the implied warranty of
"              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
"              General Public License for more details.
"
"              You should have received a copy of the GNU General Public License
"              along with this program. If not, see
"              <http://www.gnu.org/licenses/>.
" ==============================================================================
" }}} Preamble

if exists('g:MWM#Library#Global#autoload')
  finish
endif

if !has('gui_running')
  finish
endif

if !has('win32') && !has('win32unix')
  finish
endif

let g:MWM#Library#Global#autoload = 1

let s:cpoptions = &cpoptions
set cpoptions&vim

" Functions {{{
" ==============================================================================
" Public {{{
" ==============================================================================
function! MWM#Library#Global#SetPath(path)
  if type(a:path) != type('')
    throw "`a:path` should be of type `String`."
  endif
  let s:path = a:path
endfunction
" ------------------------------------------------------------------------------
function! MWM#Library#Global#GetPath()
  return simplify(expand(s:path, 1))
endfunction
" ------------------------------------------------------------------------------
function! MWM#Library#Global#GetAbsolutePath()
  return fnamemodify(MWM#Library#Global#GetPath(), ':p')
endfunction
" ------------------------------------------------------------------------------
function! MWM#Library#Global#IsAbsolute()
  return
  \ MWM#Library#Global#GetPath()
  \ ==
  \ MWM#Library#Global#GetAbsolutePath()
endfunction
" ------------------------------------------------------------------------------
function! MWM#Library#Global#Exists()
  try
    return
    \ libcall(
    \   MWM#Library#Global#GetPath(),
    \   'Vim_MWM_Version_ToString',
    \   0
    \ )
    \ ==
    \ MWM#Version#ToString()
  catch
  endtry
  return 0
endfunction
" ------------------------------------------------------------------------------
function! MWM#Library#Global#Call(function, argument)
  if MWM#Library#Global#Exists()
    try
      return libcall(MWM#Library#Global#GetPath(), a:function, a:argument)
    catch
      throw
      \ "Cannot call function:"                                                .
      \ ' '                                                                    .
      \ '`'                                                                    .
      \ a:function                                                             .
      \ '('                                                                    .
      \ a:argument                                                             .
      \ ')'                                                                    .
      \ '`'                                                                    .
      \ ' '                                                                    .
      \ "from global library:"                                                 .
      \ ' '                                                                    .
      \ '"'                                                                    .
      \ MWM#Library#Global#GetPath()                                           .
      \ '"'                                                                    .
      \ '.'
    endtry
  else
    throw
    \ "Cannot find global library:"                                            .
    \ ' '                                                                      .
    \ '"'                                                                      .
    \ MWM#Library#Global#GetPath()                                             .
    \ '"'                                                                      .
    \ '.'
  endif
endfunction
" ==============================================================================
" }}} Public
" ==============================================================================
" }}} Functions

" Defaults {{{
" ==============================================================================
call MWM#Library#Global#SetPath('vim-mwm')
" ==============================================================================
" }}} Defaults

let &cpoptions = s:cpoptions
unlet s:cpoptions

" Modeline {{{
" ==============================================================================
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ==============================================================================
" }}} Modeline
