" Preamble {{{
" ==============================================================================
"        File: Library.vim
" ------------------------------------------------------------------------------
"     Version: 0.0.0
" ------------------------------------------------------------------------------
"     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
" Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"     License: This program is free software: you can redistribute it and/or
"              modify it under the terms of the GNU General Public License as
"              published by the Free Software Foundation, either version 3 of
"              the License, or (at your option) any later version.
"
"              This program is distributed in the hope that it will be useful,
"              but WITHOUT ANY WARRANTY; without even the implied warranty of
"              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
"              General Public License for more details.
"
"              You should have received a copy of the GNU General Public License
"              along with this program. If not, see
"              <http://www.gnu.org/licenses/>.
" ==============================================================================
" }}} Preamble

if exists('g:MWM#Library#autoload')
  finish
endif

if !has('gui_running')
  finish
endif

if !has('win32') && !has('win32unix')
  finish
endif

let g:MWM#Library#autoload = 1

let s:cpoptions = &cpoptions
set cpoptions&vim

" Functions {{{
" ==============================================================================
" Public {{{
" ==============================================================================
function! MWM#Library#GetPath()
  if     MWM#Library#Local#Exists()
    return MWM#Library#Local#GetPath()
  elseif MWM#Library#Global#Exists()
    return MWM#Library#Global#GetPath()
  else
    return ''
  endif
endfunction
" ------------------------------------------------------------------------------
function! MWM#Library#GetAbsolutePath()
  if     MWM#Library#Local#Exists()
    return MWM#Library#Local#GetAbsolutePath()
  elseif MWM#Library#Global#Exists()
    return MWM#Library#Global#GetAbsolutePath()
  else
    return ''
  endif
endfunction
" ------------------------------------------------------------------------------
function! MWM#Library#Exists()
  return
  \ MWM#Library#Local#Exists()
  \ ||
  \ MWM#Library#Global#Exists()
endfunction
" ------------------------------------------------------------------------------
function! MWM#Library#Call(function, argument)
  try
    return MWM#Library#Local#Call(a:function, a:argument)
  catch /call/
    throw v:exception
  catch
    let l:exception = v:exception
  endtry
  try
    return MWM#Library#Global#Call(a:function, a:argument)
  catch /call/
    throw v:exception
  catch
    let l:exception .= ' ' . v:exception
  endtry
  throw l:exception
endfunction
" ------------------------------------------------------------------------------
function! MWM#Library#Check()
  if     MWM#Library#Local#Exists()
    echomsg
    \ "MWM:"                                                                   .
    \ ' '                                                                      .
    \ "Using local library:"                                                   .
    \ ' '                                                                      .
    \ '"'                                                                      .
    \ MWM#Library#Local#GetPath()                                              .
    \ '"'                                                                      .
    \ '.'
  elseif MWM#Library#Global#Exists()
    echomsg
    \ "MWM:"                                                                   .
    \ ' '                                                                      .
    \ "Using global library:"                                                  .
    \ ' '                                                                      .
    \ '"'                                                                      .
    \ MWM#Library#Global#GetPath()                                             .
    \ '"'                                                                      .
    \ '.'
  else
    echohl WarningMsg
    echomsg
    \ "MWM: Warning:"                                                          .
    \ ' '                                                                      .
    \ "Cannot find local library:"                                             .
    \ ' '                                                                      .
    \ '"'                                                                      .
    \ MWM#Library#Local#GetPath()                                              .
    \ '"'                                                                      .
    \ '.'                                                                      .
    \ ' '                                                                      .
    \ "Cannot find global library:"                                            .
    \ ' '                                                                      .
    \ '"'                                                                      .
    \ MWM#Library#Global#GetPath()                                             .
    \ '"'                                                                      .
    \ '.'
    echohl None
  endif
endfunction
" ==============================================================================
" }}} Public
" ==============================================================================
" }}} Functions

let &cpoptions = s:cpoptions
unlet s:cpoptions

" Modeline {{{
" ==============================================================================
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ==============================================================================
" }}} Modeline
